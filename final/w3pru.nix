with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "w3af";
  
  src = fetchgit {
    url = "https://github.com/andresriancho/w3af.git";
    sha256 = "1bj42nwa8d6hh7zswfb8hmi1fn6r61zk2yp0vp36krbq8ma0sccm";
  };
  buildInputs = [ pkgs.python27 pkgs.python27Packages.pip pkgs.python27Packages.cffi pkgs.openssl pkgs.libxml2 pkgs.libxslt pkgs.python27Packages.pillow ];
   
    
  installPhase =
    ''
 export PATH=$PWD/venv/bin:$PATH
  export HOME=$(mktemp -d) 
   pip install pyClamd==0.3.15 PyGithub==1.21.0 GitPython==2.1.3  pybloomfiltermmap==0.3.14 esmre==0.3.1 phply==0.9.1 nltk==3.0.1 chardet==2.1.1 tblib==0.2.0 pdfminer==20140328 futures==2.1.5 pyOpenSSL==0.15.1 ndg-httpsclient==0.3.3 pyasn1==0.1.9 lxml==3.4.4 scapy-real==2.2.0-dev guess-language==0.2 cluster==1.1.1b3 msgpack-python==0.4.4 python-ntlm==1.0.1 halberd==0.2.4 darts.util.lru==0.5 Jinja2==2.7.3 vulndb==0.0.19 markdown==2.6.1 psutil==2.2.1 termcolor==1.1.0 mitmproxy==0.13 ruamel.ordereddict==0.4.8 Flask==0.10.1 PyYAML==3.12 tldextract==1.7.2
    ./w3af_console 
    yes 
    ''; 

  meta = {
    homepage = http://w3af.org/download;
    description = "The project’s goal is to create a framework to help you secure your web applications by finding and exploiting all web application vulnerabilities.";
  };
}
