Repositorio en el que se probara nix

Estas son las caracteristicas especiales con la que la maquina virtual en la que corre NixOs esta configurada.<br />

Especificaciones de la maquina virtual:<br />
Sistema Operativo: Linux 2.6/3.x/4.x(64 Bits)<br />
Ram: 1,5 Gbs<br />
Chipset:PIIX3<br />
Procesadores: 1<br />
Limite de ejecucion: 100%<br />
memoria de video: 32Mb<br />
Disco duro:<br />
   Formato: Normal(VMDK)<br />
   Tipo:AHCI<br />
   Puertos: 4<br />
Red: NAT<br />

Caracteristicas Extendidas: 
-Habilitar: I/O APIC, Reloj hardware en Tiempo UTC, PAE/NX, paginacion anidada, cache de I/O anfitrion 
