{fetchurl ? (import <nixpkgs> {}).fetchurl
}:
     fetchurl {
	url="https://d9hhrg4mnvzow.cloudfront.net/www.fluid.la/1tpdjjj-hacker-17_0gc08y0dm08y02k000.png";
	sha256="1qgnxs97427dshqx26j69iy4fb2gj8m86k182fwng8ybfd39n6sv";
     }
